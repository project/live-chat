<?php
/**
 * @file
 * Drupal Module: csupport
 * Adds an inline live chat button powered by cSupport
 * to the bottom of all your Drupal pages.
 */

/**
 * Implements hook_help().
 */
function csupport_help($path, $arg) {
  switch ($path) {
    case 'admin/config/system/csupport':
      return t('This module implements an inline Live Chat button powered by cSupport. Need help? Check our <a href="@url" target="_blank">knowledge base</a>.', array('@url' => 'https://csupporthq.com/knowledge-base/'));
  }
}

/**
 * Implements hook_menu().
 */
function csupport_menu() {
  $items['admin/config/system/csupport'] = array(
    'title' => 'cSupport Live Chat',
    'description' => 'Configure the settings used to generate your cSupport code.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('csupport_admin_settings_form'),
    'access arguments' => array('administer csupport'),
    'file' => 'csupport.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function csupport_permission() {
  return array(
    'administer csupport' => array(
      'title' => t('Administer cSupport Live Chat module'),
      'description' => t('Permission to change cSupport Live Chat module settings'),
    ),
  );
}

/**
 * Implements hook_page_alter().
 */
function csupport_page_alter(&$page) {
  global $user;
  $domain = check_plain(variable_get('csupport_id', ''));

  // Verify that there is a valid ID, that the user should see the livechat
  // and the page should include this live chat.
  if (!empty($domain) && _csupport_role_can_see($user) && _csupport_page_should_show()) {

    // Loading settings.
    $type = check_plain(variable_get('csupport_type', 'chat-float-inline'));
    $autofill = check_plain(variable_get('csupport_autofill', 1));
    $autostart = check_plain(variable_get('csupport_autostart', 0));
    $margin = check_plain(variable_get('csupport_margin', ''));
    $position = check_plain(variable_get('csupport_position', 'b-r'));
    $bgcolor = check_plain(variable_get('csupport_bgcolor', ''));

    // Building base tag.
    $script = '(function(d,c){var scrs=d.getElementsByTagName("script");var scr=scrs[scrs.length-1];var e=d.createElement("script");e.async=true;e.src=("https:"==document.location.protocol?"https://":"http://")+"' . $domain . '/external/' . $type . '.js?"+scrs.length;if(typeof c=="function")if(e.addEventListener)e.addEventListener("load",c,false);else if(e.readyState)e.onreadystatechange=function(){if(this.readyState=="loaded")c();};scr.parentNode.insertBefore(e,scr);})(document,null);';

    // Auto-fill form settings. Will add name and email to the prepopulation
    // form.
    if ($autofill == 1 && $user->uid) {
      $script .= 'var cs_name="' . ($user->name) . '";var cs_email="' . ($user->mail) . '";';

      // Auto-start chat session setting, to start the chat automatically.
      if ($autostart == 1) {
        $script .= 'var cs_autostart=true;';
      }
    }

    // Add margin settings for the chat button. Only numeric values can be
    // used, and only in certain range.
    if (is_numeric($margin)) {
      if ($margin > 100) {
        $margin = 100;
      }
      elseif ($margin < -10) {
        $margin = -10;
      }

      // To prevent malform such as 1.23, we just take the integer
      // value with intVal().
      $script .= 'var cs_margin="' . intval($margin) . 'px";';
    }

    // Add position settings, if valid.
    if ($position == 't-l' || $position == 't-r' || $position == 'b-l' || $position == 'b-r') {
      $script .= 'var cs_position="' . $position . '";';
    }

    // Add color setting. Two version #000000 and #000000/#FFFFFF.
    if (preg_match('/^#[a-fA-F0-9]{6}(\/#[a-fA-F0-9]{6})?$/i', $bgcolor)) {
      $script .= 'var cs_bgcolor="' . $bgcolor . '";';
    }

    drupal_add_js($script, array('type' => 'inline', 'scope' => 'footer'));

    // Make sure overlay will stay over the chat.
    $css = 'span[id^="chat_popup"],div[id^="outer-frame"]{z-index:499 !important;}';
    drupal_add_css($css, array('type' => 'inline', 'scope' => 'header'));
  }
}

/**
 * Check if user can see the live chat or not.
 */
function _csupport_role_can_see($user) {
  $roles_set = variable_get('csupport_visibility_roles', array());

  // Loop through the roles of the user.
  foreach ($user->roles as $id => $role) {
    if (isset($roles_set[$id]) && $roles_set[$id] == $id) {
      // Exclude the live chat for this role/user.
      return FALSE;
    }
  }

  // User should see the live chat.
  return TRUE;
}

/**
 * Check if page should show the live chat or not.
 */
function _csupport_page_should_show() {
  $pages = variable_get('csupport_visibility_pages', implode(PHP_EOL, array(
    'admin',
    'admin/*',
    'user/*/*',
    'node/add*',
    'node/*/*')));
  // Match path if necessary.
  if (drupal_strlen($pages) > 0) {
    $visibility_pages_type = variable_get('csupport_visibility_pages_type', 0);
    $found = drupal_match_path(drupal_get_path_alias($_GET['q']), $pages);
    if (!$found && $visibility_pages_type == 1) {
      // Page is not in include list.
      return FALSE;
    }
    elseif ($found && $visibility_pages_type == 0) {
      // Page is in exclude list.
      return FALSE;
    }
  }
  // Page should include the live chat.
  return TRUE;
}
